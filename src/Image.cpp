#include "Image.h"
#include "Pixel.h"
#include <iostream>
#include <string.h>
#include <fstream>
#include <cstdio>
#include <cassert>
using namespace std;

Image::Image()
{
    dimx=0;
    dimy=0;
    tab= new Pixel[1];
}

Image::Image(int dimensionX,int dimensionY)
{
    dimx=dimensionX;
    dimy=dimensionY;
    tab = new Pixel[dimx * dimy];
}

Image::~Image()
{
	dimx=0;
	dimy=0;
    delete [] tab; 
}

Pixel& Image::getPix(int x,int y)
{
	if (x >= 0 && x < dimx && y >= 0 && y < dimy) 
	{
    return tab[y*dimx+x];
	}
	else std::cout<<"le pixel n'existe pas"<<std::endl;
	return tab[0];
}

const Pixel Image::getPix(int x,int y) const
{
	if((x*y<=dimx*dimy)&&(x*y>=0))
	{
    return tab[y*dimx+x];
	}
	else std::cout<<"le pixel n'existe pas"<<std::endl;
	return Pixel();
}

void Image::setPix(int x,int y,Pixel couleur)
{
	if (x >= 0 && x < dimx && y >= 0 && y < dimy) {
    tab[y*dimx+x]=couleur;
	}
}   

void Image::dessinerRectangle(int Xmin,int Ymin,int Xmax,int Ymax,Pixel couleur)
{
    int i,j;
    for(i=Xmin;i<=Xmax;i++)
    {
         for(j=Ymin;j<=Ymax;j++)
         {
            setPix(i,j,couleur);
         }
    }
}

void Image::effacer(Pixel couleur)
{
    dessinerRectangle(0,0,dimx,dimy,couleur);
}

void Image::testRegression()
{
    Pixel p1;
    assert(p1.r==0);
    assert(p1.g==0);
    assert(p1.b==0);
    
    p1 = Pixel(4,12,5);
    assert(p1.r==4);

    assert(p1.g==12);
    assert(p1.b==5);

    Image im1;
    assert(im1.dimx==0);
    assert(im1.dimy==0);

    Image im2(50,100);
    assert(im2.dimx==50);
    assert(im2.dimy==100);

    im2.setPix(8,7,p1);
    
    assert(im2.getPix(8,7).r==p1.r);
    assert(im2.getPix(8,7).g==p1.g);
    assert(im2.getPix(8,7).b==p1.b);

    im2.dessinerRectangle(0,0,50,100,p1);
    int i,j;
    for(i=0;i<25;i++)
    {
         for(j=0;j<50;j++)
         {
            assert(im2.getPix(i,j).r==p1.r);
            assert(im2.getPix(i,j).g==p1.g);
            assert(im2.getPix(i,j).b==p1.b);
         }
    }

    Pixel p2;
    p2.r=255;
    p2.g=0;
    p2.b=0;
    im2.setPix(5,4,p2);
    im2.effacer(p2);
    int k,h;
    for(k=0;k<im2.dimx;k++)
    {
         for(h=0;h<im2.dimy;h++)
         {
            assert(im2.getPix(k,h).r==p2.r);
            assert(im2.getPix(k,h).g==p2.g);
            assert(im2.getPix(k,h).b==p2.b);
         }
    } 
    
}


/****************************td3************************************/


void Image::sauver(const string &filename) const
{
    ofstream fichier(filename.c_str());
    assert(fichier.is_open());
    fichier << "P3" << endl;
    fichier << dimx << " " << dimy << endl;
    fichier << "255" << endl;
    for (int y = 0; y < dimy; ++y)
        for (int x = 0; x < dimx; ++x)
        {
            const Pixel &pix = getPix(x, y);
            fichier << +pix.r << " " << +pix.g << " " << +pix.b << " ";
        }
    cout << "Sauvegarde de l'image " << filename << " ... OK\n";
    fichier.close();
}

void Image::ouvrir(const string &filename)
{
    ifstream fichier(filename.c_str());
    assert(fichier.is_open());
    char r, g, b;
    string mot;
    dimx = dimy = 0;
    fichier >> mot >> dimx >> dimy >> mot;
    assert(dimx > 0 && dimy > 0);
    if (tab != nullptr)
        delete[] tab;
    tab = new Pixel[dimx * dimy];
    for (int y = 0; y < dimy; ++y)
        for (int x = 0; x < dimx; ++x)
        {
            fichier >> r >> g >> b;
            getPix(x, y).r = r;
            getPix(x, y).g = g;
            getPix(x, y).b = b;
        }
    fichier.close();
    cout << "Lecture de l'image " << filename << " ... OK\n";
}

void Image::afficherConsole()
{
    cout << dimx << " " << dimy << endl;
    for (int y = 0; y < dimy; ++y)
    {
        for (int x = 0; x < dimx; ++x)
        {
            Pixel &pix = getPix(x, y);
            cout << +pix.r << " " << +pix.g << " " << +pix.b << " ";
        }
        cout << endl;
    }
}

int Image::getDimX() const {
    return dimx;
}

int Image::getDimY() const {
    return dimy;
}
Pixel* Image::getTab() const {
    return tab;
}




