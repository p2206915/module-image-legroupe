#ifndef _IMAGEVIEWER_H
#define _IMAGEVIEWER_H
#include "Image.h"
#include "Pixel.h"
#include <SDL2/SDL.h>


/**
 * @class ImageViewer
 * @brief  
 */
class ImageViewer{
    private:
    	SDL_Window* window;
    	SDL_Renderer* renderer;
    public:
		/**
     		* @brief Constructeur qui initialise tout SDL2 et crée la fenêtre
			* et le renderer     		
     		*/
	ImageViewer();
		/**
     		* @brief Détruit et ferme SDL2.
			*/
	~ImageViewer();
// 
	 /**
     		* @brief Affiche l’image passée en paramètre et permet le (dé)zoom.
     		* Appelle dessinerRectangle avec les coordonnées du rectangle correspondant à toute l'image.
     		* @param I
     		*/
	void afficher (const Image& im);
};

#endif