#ifndef _IMAGE_H
#define _IMAGE_H
#include <string>
#include "Pixel.h"

/**
 * @class Image
 * @brief Représente une image composée de pixels 
 */
class Image{
    private:
        Pixel* tab; ///< Le tableau 1D de Pixel.
        int dimx, dimy; ///< Les dimensions de l'image.
    public:
    int getDimX() const; // Méthode pour récupérer la dimension X
    int getDimY() const; // Méthode pour récupérer la dimension Y
    Pixel* getTab() const;
        /**
     		* @brief Constructeur par défaut de la classe Image.
     		* Initialise dimx et dimy à 0.
     		* Ne alloue pas de mémoire pour le tableau de pixels.
     		*/
Image ();
        /**
     		* @brief Constructeur de la classe Image.
     		* Initialise dimx et dimy (après vérification)
     		* Alloue le tableau de pixels dans le tas (image noire).
     		* @param dimensionX La dimension X de l'image.
     		* @param dimensionY La dimension Y de l'image.
     		*/
Image (int dimensionX,int dimensionY);
        /**
     		* @brief Destructeur de la classe Image.
     		* Désalloue la mémoire du tableau de pixels
     		* Met à jour les champs dimx et dimy à 0.
     		*/
~Image ();
        /**
     		* @brief Récupère le pixel original de coordonnées (x,y) en vérifiant sa validité.
     		* Passe d'un tableau 2D à un tableau 1D.
     		* @param x La coordonnée X du pixel.
     		* @param y La coordonnée Y du pixel.
     		* @return Une référence vers le pixel original.
     		*/
Pixel& getPix (int x,int y); 
        /**
     		* @brief Récupère une copie du pixel original de coordonnées (x,y) en vérifiant sa validité.
     		* @param x La coordonnée X du pixel.
     		* @param y La coordonnée Y du pixel.
     		* @return Une copie du pixel original.
     		*/
const Pixel getPix (int x,int y) const;
        /**
     		* @brief Modifie le pixel de coordonnées (x,y).
     		* @param x La coordonnée X du pixel.
     		* @param y La coordonnée Y du pixel.
     		* @param couleur La couleur à assigner au pixel.
     		*/
void setPix (int x,int y,Pixel couleur);
        /**
     		* @brief Dessine un rectangle plein de la couleur dans l'image.
     		* Utilise setPix pour remplir les pixels du rectangle.
     		* @param Xmin La coordonnée X minimale du rectangle.
     		* @param Ymin La coordonnée Y minimale du rectangle.
     		* @param Xmax La coordonnée X maximale du rectangle.
     		* @param Ymax La coordonnée Y maximale du rectangle.
     		* @param couleur La couleur du rectangle.
     		*/
void dessinerRectangle(int Xmin,int Ymin,int Xmax,int Ymax,Pixel couleur);
        /**
     		* @brief Efface l'image en la remplissant de la couleur spécifiée.
     		* Appelle dessinerRectangle avec les coordonnées du rectangle correspondant à toute l'image.
     		* @param couleur La couleur à utiliser pour effacer l'image.
     		*/
void effacer (Pixel couleur);
        /**
     		* @brief Effectue une série de tests pour vérifier le bon fonctionnement des méthodes de la classe Image.
     		* Vérifie également que les données membres de l'objet sont conformes.
     		*/
static void testRegression ();

void sauver(const std::string &filename) const;

void ouvrir(const std::string &filename);

void afficherConsole();


};

#endif
