#include "ImageViewer.h"
#include "Image.h"
#include <SDL2/SDL.h>
#include <iostream>
float zoomFactor = 1.2f;
float dezoomFactor = 0.8f;
ImageViewer::ImageViewer() : window(nullptr), renderer(nullptr) {
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        std::cerr << "SDL initialization failed: " << SDL_GetError() << std::endl;
    }

    window = SDL_CreateWindow("Image Viewer", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 200, 200, SDL_WINDOW_SHOWN);
    if (window == nullptr) {
        std::cerr << "Window creation failed: " << SDL_GetError() << std::endl;
    }

    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == nullptr) {
        std::cerr << "Renderer creation failed: " << SDL_GetError() << std::endl;
    }
}

ImageViewer::~ImageViewer() {
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void ImageViewer::afficher(const Image& im) {
    // Convertir le tableau de pixels de l'image en une SDL_Surface
    SDL_Surface* surface = SDL_CreateRGBSurfaceFrom((void*)im.getTab(), im.getDimX(), im.getDimY(), 24, im.getDimX() * 3, 0xFF0000, 0x00FF00, 0x0000FF, 0);
    if (surface == nullptr) {
        std::cerr << "SDL surface creation failed: " << SDL_GetError() << std::endl;
        return;
    }

    SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, surface);
    if (texture == nullptr) {
        std::cerr << "SDL texture creation failed: " << SDL_GetError() << std::endl;
        SDL_FreeSurface(surface);
        return;
    }

    SDL_FreeSurface(surface);

    // Effacer le renderer
    SDL_SetRenderDrawColor(renderer, 200, 200, 200, 255);
    SDL_RenderClear(renderer);

    // Afficher la texture au centre de la fenêtre
    SDL_Rect dstRect = {(200 - im.getDimX()) / 2, (200 - im.getDimY()) / 2, im.getDimX(), im.getDimY()};
    SDL_RenderCopy(renderer, texture, NULL, &dstRect);
    SDL_RenderPresent(renderer);

    // Boucle d'événements
    bool running = true;
    SDL_Event event;
    while (running) {
        while (SDL_PollEvent(&event)) {
            if (event.type == SDL_QUIT) {
                running = false;
            }
            else if (event.type == SDL_KEYDOWN) {
                if (event.key.keysym.sym == SDLK_ESCAPE) {
                    running = false;
                }
                else if (event.key.keysym.sym == SDLK_t) {
                }
                else if (event.key.keysym.sym == SDLK_g) {
                }
            }
        }
    }

    SDL_DestroyTexture(texture);
}
