#ifndef _PIXEL_H
#define _PIXEL_H
/**
 * @struct Pixel
 *
 * @brief Structure pixel qui contient 3 unsigned char représentant les couleurs RGB.Il y a également 2 constructeurs.
 *
 * Une classe pour illustrer l'utilisation de Doxygen pour générer de la documentation.
 */

struct Pixel{
	
	unsigned char r,g,b; ///< 3 couleurs r g b pour Red Green Blue
	
	/**
 	* @brief Constructeur par défaut. 
 	* Initialise les couleurs r g b à 0.
 	*/
	Pixel();
	
	/**
 	* @brief Description de la fonction.
 	* 
 	* @param nr valeur du canal rouge
 	* @param ng valeur du canal vert
 	* @param nb valeur du canal bleu
 	*/
	Pixel(unsigned char nr,unsigned char ng,unsigned char nb);
};

#endif
