all: bin/test bin/exemple bin/affichage

bin/test : obj/mainTest.o obj/Pixel.o obj/Image.o
	g++ -Wall obj/mainTest.o obj/Image.o obj/Pixel.o -o bin/test
	
bin/affichage : obj/mainAffichage.o obj/Pixel.o obj/Image.o obj/ImageViewer.o
	g++ -Wall obj/mainAffichage.o obj/Image.o obj/Pixel.o obj/ImageViewer.o -o bin/affichage -lSDL2
	
bin/exemple : obj/mainExemple.o obj/Pixel.o obj/Image.o
	g++ -Wall obj/mainExemple.o obj/Image.o obj/Pixel.o -o bin/exemple

obj/Image.o : src/Image.cpp
	g++ -Wall -c src/Image.cpp -o obj/Image.o

obj/Pixel.o : src/Pixel.cpp
	g++ -Wall -c src/Pixel.cpp -o obj/Pixel.o
	
obj/ImageViewer.o : src/ImageViewer.cpp
	g++ -Wall -c src/ImageViewer.cpp -o obj/ImageViewer.o -lSDL2
	
obj/mainTest.o : src/mainTest.cpp
	g++ -Wall -c src/mainTest.cpp -o obj/mainTest.o
	
obj/mainExemple.o : src/mainExemple.cpp
	g++ -Wall -c src/mainExemple.cpp -o obj/mainExemple.o
	
obj/mainAffichage.o : src/mainAffichage.cpp
	g++ -Wall -c src/mainAffichage.cpp -o obj/mainAffichage.o

doc: doc/doxyfile
	doxygen doc/doxyfile

exemple:
	bin/exemple

clean:
	rm -r obj/*.o bin/exemple 
	
